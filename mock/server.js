/* eslint-disable  no-console */
const express = require('express');
const axios = require('axios');
const path = require('path');
const fs = require('fs');
const dataMockFile = path.resolve(__dirname, 'data.mock.json');
const mockData = require(dataMockFile);

const allowedLaunchParams = [
    'save',
    'dontUseMocks',
    'apiHost',
    'apiPort',
    'port'
];

/**
 * @type LaunchParams
 */
const launchParams = require('minimist')(process.argv.slice(2), {
    string: [
        'apiHost',
        'apiPort',
        'severHost',
        'port'
    ],
    boolean: ['dontUseMocks', 'save']
});

/**
 * В случае если передали недозволенный параметр, обрываем процесс
 */
Object.keys(launchParams).slice(1).forEach(param => {
    if (!allowedLaunchParams.includes(param)) {
        console.log(`\nНедопустимый параметр при запуске "${param}"`);
        console.log(`Используй один из:
            \n--${allowedLaunchParams.join('\n--')} \n`
        );
        process.exit();
    }
});

const dontUseMocks = launchParams.dontUseMocks;
const save = launchParams.save;
const apiHost = launchParams.apiHost || 'http://194.87.239.90';
const apiPort = parseInt(launchParams.apiPort, 10) || 8081;
const port = parseInt(launchParams.port, 10) || 3000;

/**
 * Обертка над фетчем данных. Сохраняет данные в data.mock.json при нужных флагах
 * @param { RequestConfig } config Конфиг запроса
 */
async function request(config) {
    const requestConfig = {
        baseURL: `${apiHost}:${apiPort}`,
        ...config
    };

    if (dontUseMocks ||
        !save ||
        !request.ALLOWED_METHODS.includes(config.method)
    ) {
        return await axios.request(requestConfig);
    }

    const { method, baseURL, url: path } = requestConfig;
    const url = `${baseURL}${path}`;

    if (mockData[method][url]) {
        return mockData[method][url];
    }

    try {
        const response = await axios.request(requestConfig);

        mockData[method][url] = {
            data: response.data,
            status: response.status,
            statusCode: response.statusCode,
            statusText: response.statusText
        };

        fs.writeFileSync(dataMockFile, JSON.stringify(mockData));

        console.log(`Дампы для запрса ${url} сохранены!`);

        return mockData[method][url];
    } catch (e) {
        console.error(`Запрос на ${url} упал со статусом ${e.response.status}${': ' && e.response.statusText}`);

        return e;
    }
}

/**
 * Массив доступных для моков методов
 */
request.ALLOWED_METHODS = ['get'];

const app = express();
app.listen(port);
console.log(`Сервер слушает порт ${port}...\n`);

app.get('/*', async function(req, res) {
    request({
        url: req.url,
        method: 'get'
    })
    .then(response => {
        res.json(response.data);
    })
    .catch(error => {
        res.sendStatus(404);
    });
  });

/**
 * @typedef LaunchParams
 * @property { boolean } [dontUseMocks]  Использовать ли моки. Если нет, просто проксирует запросы на АПИ
 * @property { boolean } [save]          Перезаписывает дампы, если есть и создает, если нет
 * @property { string }  [apiHost]       Адрес API с которого собираются моки
 * @property { string }  [apiPort]       Порт API
 * @property { string }  [severHost]     Хост для запуска сревера
 * @property { string }  [port]          Порт, на котором стартует сервер
 */

 /**
  * @typedef RequestConfig
  * @property { string } url       Путь, который может быть закеширован
  * @property { 'get' }  [method]  Один из доступных методов запроса
  * @property { object } [data]    Тело запроса
  * @property { object } [headers] Заголовки
  */
