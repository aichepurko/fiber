const config = require('../webpack/configs/development');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    module: {...config.module},
    // Не переопределяем, иначе он проводит рендер страницы и перетирает весь сторибук
    plugins: [...config.plugins, ].filter(plugin => !(plugin instanceof HtmlWebpackPlugin)),
    resolve: config.resolve
}
