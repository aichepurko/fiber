import { configure, addDecorator } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import { withOptions } from '@storybook/addon-options';

const req = require.context('../src/', true, /stories\.tsx$/);

addDecorator(withInfo({ inline: true }));

addDecorator(withOptions({ }));

configure(function() { req.keys().forEach(filename => req(filename)); }, module);
