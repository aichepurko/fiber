#!/bin/bash

# Первый аргумент - линтер
# Остаток - файлы на проверку
lint() {
    for ((i = 2; i <= $#; i++ )); do
        file=${!i}
        git show ":$file" | node_modules/.bin/$1 -- $file
        if [ $? -ne 0 ]; then
            ERROR=1
        fi
    done
    if [ $ERROR ]
    then
        echo "...( ͡ಠ ʖ̯ ͡ಠ)... \n"
        echo "Не огорчай линтер^ сделай по-красоте \n"
        echo "Чтобы ты не грустил, вот тебе котенок \n (\__/) \n (='.'=) \n  ("\)_\(") "
        return 1
    else
        echo "⚡️  Тебя благословляет $1! ⚡️"
    fi

}
