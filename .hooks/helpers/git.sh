# Первый параметр - регэксп на расширения файлов на греп
# Пример $(diffFiles "ts|tsx")
diffFiles() {
    echo $(git diff --cached --name-only --diff-filter=ACMR | grep -E '\.('$1')$')
}

BRANCH_NAME=$(git rev-parse --abbrev-ref HEAD)
