module.exports = {
    gridUrl: 'http://194.87.239.90:4444/wd/hub',
    sets: {
        desktop: {
            files: '__tests__'
        }
    },

    browsers: {
        chrome: {
            desiredCapabilities: {
                browserName: 'chrome' // this browser should be installed on your OS
            }
        }
    }
};

// TODO:  скрипты для селениум грида на сервере
// java -Dwebdriver.chrome.driver=chromedriver.exe -jar selenium-server-standalone-3.141.59.jar -role webdriver -hub http://localhost:4444/grid/register -port 5558 -browser browserName=chrome


// java -Dwebdriver.chrome.driver=chromedriver.exe -jar selenium-server-standalone-2.46.0.jar -role webdriver -hub http://localhost:4444/grid/register -port 5558 -browser browserName=chrome


// java -jar selenium-server-standalone-2.42.0.jar -role node -browser browserName=chrome -hub http://localhost:4444/grid/register
