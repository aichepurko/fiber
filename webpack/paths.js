const path = require('path');

const root = path.resolve(__dirname, '../');

/**
 *
 * @param {String} p Путь относительно корня проекта
 * @returns {func}
 * @example resolve('package.json')
 */
const resolve = p => path.resolve(root, p);

module.exports = {
    root,
    resolve,
    config: {
        ts: resolve('tsconfig.json')
    },
    lint: {
        ts: resolve('tslint.json')
    },
    package: resolve('package.json'),
    src: resolve('src'),
    htmlTemplate: resolve('src/index.html'),
    appEntry: resolve('src/index.tsx'),
    buildDir: resolve('.build')
};
