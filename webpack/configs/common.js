const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');

const {
    root,
    tslint,
    tsconfig,
    src: srcRoot,
    htmlTemplate,
    appEntry,
    buildDir
} = require('../paths');

const plugins = [
    // new webpack.ProgressPlugin(),
    new HtmlWebpackPlugin({ template: htmlTemplate }),
    new webpack.DefinePlugin({}),
    new ForkTsCheckerWebpackPlugin({
        async: false,
        watch: srcRoot,
        tsconfig,
        tslint
      })
];

const rules = [{
    test: /\.(ts|tsx)$/,
    include: srcRoot,
    use: [{
        loader: 'ts-loader',
        options: {
          transpileOnly: true
        }
    }]
}, {
    test: /\.(css|pcss)$/,
    use: [
        'style-loader',
        'css-loader',
        {
            loader: 'postcss-loader',
            options: {
                sourceMap: true,
                config: {
                    path: root
                },
                plugins: [
                    require('stylelint')({
                        configFile: path.resolve(root, '.stylelintrc.js')
                    }),
                    require('postcss-import')(),
                    require('postcss-preset-env')(),
                    require('cssnano')(),
                    require('postcss-nested')()
                ]
            }
        }
    ]
  }, {
    test: /\.(gif|png|jpe?g|svg)$/i,
    use: [
      'file-loader',
      {
        loader: 'image-webpack-loader',
        options: {
          mozjpeg: {
            // progressive: true,
            quality: 65
          },
          // optipng.enabled: false will disable optipng
          optipng: {
            enabled: false
          },
          pngquant: {
            quality: '65-90',
            speed: 4
          },
          gifsicle: {
            interlaced: false
          },
          // the webp option will enable WEBP
          webp: {
            quality: 75
          }
        }
      }
    ]
  }
];

const extensions = [
    '.ts',
    '.tsx',
    '.js',
    '.pcss',
    '.css',
    '.svg'
];

module.exports = {
    plugins,
    rules,
    extensions,
    config: {
        entry: appEntry,
        output: {
            path: buildDir,
            filename: 'app.bundle.[hash].js'
        },
        plugins,
        resolve: {
            extensions,
            alias: {
                src: srcRoot
            }
        },
        module: {
            strictExportPresence: true,
            rules
        }
    }
};
