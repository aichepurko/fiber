const { name: projectName } = require(require('../paths').package);
const common = require('./common');

module.exports = {
    mode: 'development',
    ...common.config,
    optimization: {
		minimize: false
	},
    devServer: {
        host: `dev.${projectName}.ru`
    }
};
