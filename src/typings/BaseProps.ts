interface IBaseProps<Mods extends object = {}> {
    className?: string;
    classMod?: Mods;
    style?: React.CSSProperties;
    children?: React.ReactNode;
}

export {
    IBaseProps
};
