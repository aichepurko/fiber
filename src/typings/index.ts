import { IBaseProps } from './BaseProps';
import { Lang } from './Locale';

export {
    IBaseProps,
    Lang
};
