import { storiesOf } from '@storybook/react';

/* tslint:disable no-any */
const atomStory = (story: string, module: any) => storiesOf(`Atoms/${story}`, module);

const moleculeStory = (story: string, module: any) => storiesOf(`Molcules/${story}`, module);

const organismStory = (story: string, module: any) => storiesOf(`Organism/${story}`, module);

const featureStory = (story: string, module: any) => storiesOf(`Features/${story}`, module);
/* tslint:enable no-any */

export {
    atomStory,
    moleculeStory,
    organismStory,
    featureStory
};
