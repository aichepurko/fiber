import React from 'react';
import { Provider } from 'react-redux';
import { NavigationBar } from 'src/features';
import { LocaleProvider, createTranslationsStore } from 'src/i18n';
import { Lang } from 'src/typings';

import { store } from 'src/store';

import './styles.pcss';

export const translations = createTranslationsStore<Lang>('ru', 'en');

const App = function() {
    return (
        <Provider store={store}>
            <LocaleProvider store={translations} lang="en">
                <div><span>React. уиии</span></div>
                <NavigationBar />
            </LocaleProvider>
        </Provider>
    );
};

export { App };
