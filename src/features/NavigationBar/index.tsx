import React from 'react';
import { cn as cnBase } from '@bem-react/classname';
import { withTranslations } from 'src/i18n';
import { Flex } from 'src/ui/organisms';
import { LinkImage } from 'src/ui/molecules/LinkImage';
import { INavigationBarProps } from './types';
import * as translations from './i18n';

import './styles';

type ImageName = 'order' | 'panel' | 'clientele' | 'orders' | 'calendar' | 'tasks' | 'analytics' | 'settings';
// Обязательно соблюдаем порядок
const imgNames: ImageName[] = ['order', 'panel', 'clientele', 'orders', 'calendar', 'tasks', 'analytics', 'settings'];
const images = imgNames.map(image => require(`./imgs/${image}`));

const cn = cnBase('NavigationBar');
const cnLink = cn('Link');

const BaseNavigationBar: React.StatelessComponent<INavigationBarProps<ImageName>> = ({ theme, i18n }) => {
    return (
        <div className={cn({ theme })}>
            <Flex classMod={{direction: 'column'}}>
                {images.map((image, i) => (
                    <LinkImage
                        key={imgNames[i]}
                        contentAlign="column"
                        src={image}
                        className={cnLink}
                    >
                        {i18n(imgNames[i])}
                    </LinkImage>
                ))}
            </Flex>
        </div>
    );
};

const NavigationBar = withTranslations<ImageName, 'navigationBar'>(translations, 'navigationBar')(BaseNavigationBar);

export {
    NavigationBar
};
