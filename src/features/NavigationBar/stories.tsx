import React from 'react';
import { featureStory } from 'src/helpers/stories';

import { NavigationBar } from '.';

const examples = featureStory('NavigationBar', module);

const image = (
  <NavigationBar/>
);

examples
  .add('Главная панель', () => (
    <div
        style={{
            height: '100vh',
            position: 'relative',
            background: 'pink'
         }}
    ><NavigationBar />
    </div>
  ));

export { image };
