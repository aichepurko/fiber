import { IBaseProps } from 'src/typings';

interface INavigationBarProps<TranslationKey> extends IBaseProps {
    theme?: 'dark';
    i18n: (key: TranslationKey) => string;
}

export {
    INavigationBarProps
};
