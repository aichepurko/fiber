const ru = {
    order: 'заказ',
    panel: 'панель',
    clientele: 'клиенты',
    orders: 'заказы',
    calendar: 'календарь',
    tasks: 'задачи',
    analytics: 'аналитика',
    settings: 'настройки'
};

export { ru };
