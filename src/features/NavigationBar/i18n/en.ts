const en = {
    order: 'order',
    panel: 'panel',
    clientele: 'clientele',
    orders: 'orders',
    calendar: 'calendar',
    tasks: 'tasks',
    analytics: 'analytics',
    settings: 'settings'
};

export { en };
