import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { hot } from 'react-hot-loader/root';
import { App } from './app';

const root = document.getElementById('root');

const Hot: React.SFC = hot(App);

ReactDOM.render(<Hot />, root);
