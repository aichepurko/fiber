import { Reducer } from 'redux';
import { IUser } from './types';

const user: Reducer<IUser> = () => ({});

export {
    user
};
