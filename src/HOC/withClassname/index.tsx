import React from 'react';
import { ClassNameFormatter } from '@bem-react/classname';

import { IBaseProps } from 'src/typings';

const withClassName =
    <Props extends IBaseProps>
    (cn: ClassNameFormatter, baseMods: Props['classMod'] = {}) =>
    (ComponentElement: React.ComponentType<Props>) => {
        const Res = (props: Props) => {
                const { className, classMod } = props;
                const finalClassName = cn({...baseMods, ...classMod}, [className]);
                return <ComponentElement {...{...props, className: finalClassName}}/>;
        };
        Res.displayName = cn();
        return Res;
    };

export {
    withClassName
};
