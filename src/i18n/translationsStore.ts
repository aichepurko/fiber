// Сейчас поддерживаем самый простой вариант, что перевод не параметризуется
type Store<Locale extends string> = Record<string, Record<Locale, string>>;

class TranslationsStore<Locale extends string> {
    private store: Store<Locale>;
    private locales: Locale[];

    registerTranslations(key: string) {
        return (translations: Record<Locale, string>) => {
            this.locales.forEach((loc: Locale) => {
                if (!this.store[key]) {
                    this.store[key] = {[loc]: translations[loc]};
                } else
                if (this.store[key][loc]) {
                    // tslint:disable-next-line no-console
                    console.error(`You are trying to override existing key ${key}`);
                } else {
                    this.store[key][loc] = translations[loc];
                }
            });
        };
    }

    mergeTranslations(one: Store<Locale>) {
        const target = this.store;
        const newStore: Store<Locale> = {};
        Object.keys(one).forEach((lang: Locale) => {
            newStore[lang] = {...target[lang], ...one[lang]};
        });
        Object.keys(target).forEach((lang: Locale) => {
            newStore[lang] = {...target[lang], ...one[lang]};
        });
        this.store = newStore;
    }

    getTranslation(loc: Locale, prefix?: string) {
        return (key: string) => {
            return this.store[ prefix ? `${prefix}/${key}` : key][loc];
        };
    }

    constructor(locales: Locale[]) {
        this.locales = locales;

        this.store = {};
    }
}

/**
 *
 * @param Список локалей, для котороых регистрируются переводы
 */
const createTranslationsStore = <Locale extends string>(...locales: Locale[]) => {
    return new TranslationsStore<Locale>(locales);
};

export {
    createTranslationsStore,
    TranslationsStore
};
