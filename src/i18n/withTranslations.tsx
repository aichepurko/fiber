import React from 'react';
import { Omit } from 'utility-types';
import { Lang } from 'src/typings';
import { LocaleContext } from './context';

type Translations<Key extends string> = Record<Lang, Record<Key, string>>;

const mapCache: Record<string, Record<string, Record<Lang, string>>> = {};

const mapTranslations = <Key extends string>(incoming: Translations<Key>, prefix: string = '') => {
    const out = {};
    Object.keys(incoming).forEach((language: Lang) => {
        Object.keys(incoming[language]).forEach(primryKey => {
            const key = Boolean(prefix) ? `${prefix}/${primryKey}` : primryKey;
            if (!out[key]) { out[key] = {}; }
            out[key][language] = incoming[language][primryKey];
        });
    });
    return out;
};

const withTranslations =
<Key extends string, P extends string | never = never>(translations: Translations<Key>, prefix?: P) =>
<SelfProps extends {i18n: (key: string, prefix: P extends never ? never : P) => string}>
(Component: React.ComponentType<SelfProps>) => {
    const res = (props: Omit<SelfProps, 'i18n'>) => {
        return (
            <LocaleContext.Consumer>
                {({ lang, store }) => {
                    {
                        const cacheName = prefix + Object.keys(translations[lang]).join('');

                        if (!mapCache[cacheName]) {
                            mapCache[cacheName] = mapTranslations<Key>(translations, prefix);
                        }
                        store.mergeTranslations(mapCache[cacheName]);
                    }
                    const i18n = store.getTranslation(lang, prefix);
                    // Результирующий HOC не должен принимать i18n prop
                    // @ts-ignore
                    return <Component {...props} i18n={i18n} />;
                }}
            </LocaleContext.Consumer>
        );
    };
    res.displayName = (Component.displayName || Component.name).replace('Base', '');

    return res;
};

export {
    withTranslations,
    mapTranslations
};
