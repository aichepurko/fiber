import { createContext } from 'react';
import { Lang } from 'src/typings';
import { createTranslationsStore, TranslationsStore } from './translationsStore';

const LocaleContext = createContext<{
    lang: Lang;
    store: TranslationsStore<Lang>;
    setLang: (lang: Lang) => void;
}>({lang: 'ru', store: createTranslationsStore('ru', 'en'), setLang: () => undefined});

export {
    LocaleContext,
};
