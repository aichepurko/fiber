import React, { useState } from 'react';
import { Lang } from 'src/typings';
import { LocaleContext } from './context';
import { TranslationsStore } from './translationsStore';

const LocaleProvider: React.StatelessComponent<{ store: TranslationsStore<Lang>, lang?: Lang }> =
({children, lang, store}) => {
    const [langState, setLangState] = useState(lang || 'ru');
    return (
        <LocaleContext.Provider
            value={{ store, lang: langState, setLang: targetLang => setLangState(targetLang) }}
        >{children}
        </LocaleContext.Provider>
    );
};

export {
    LocaleProvider
};
