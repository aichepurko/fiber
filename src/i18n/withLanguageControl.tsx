import React from 'react';
import { Omit } from 'utility-types';
import { Lang } from 'src/typings';
import { LocaleContext } from './context';

const withLanguageControl =
<SelfProps extends {setLang: (lang: Lang) => void}>(Component: React.ComponentType<SelfProps>) =>
(props: Omit<SelfProps, 'setLang'>) => {
    return (
        <LocaleContext.Consumer>
            {({ setLang }) => {
                // Результирующий HOC не должен принимать i18n prop
                // @ts-ignore
                return <Component {...props} setLang={setLang} />;
            }}
        </LocaleContext.Consumer>
    );
};

export { withLanguageControl };
