 import { createTranslationsStore } from '../translationsStore';

 describe('createTranslationsStore', function() {
    test('Создание пустого стора', () => {
        const store = createTranslationsStore('ru', 'en');
        // @ts-ignore
        expect(store.store).toEqual({});
    });

    test('Добавление новых переводов', () => {
        const store = createTranslationsStore('ru', 'en');
        const translations = {
            en: 'test',
            ru: 'тест'
        };
        store.registerTranslations('test')(translations);
        // @ts-ignore
        expect(store.store).toEqual({
            test: {
                en: 'test',
                ru: 'тест'
            }
        });
    });

    test('Мерж переводов', () => {
        const store = createTranslationsStore('ru', 'en');
        const translations = {
            en: 'test',
            ru: 'тест'
        };
        store.registerTranslations('test')(translations);
        store.mergeTranslations({
            another: {
                ru: 'еще',
                en: 'antoher'
            }
        });
        // @ts-ignore
        expect(store.store).toEqual({
            test: {
                en: 'test',
                ru: 'тест'
            },
            another: {
                ru: 'еще',
                en: 'antoher'
            }
        });
    });

    test('Мерж переводов с пересечениями', () => {
        const store = createTranslationsStore('ru', 'en');
        const translations = {
            en: 'test',
            ru: 'тест'
        };
        store.registerTranslations('test')(translations);

        store.mergeTranslations({
            another: {
                ru: 'еще',
                en: 'antoher'
            }
        });

        store.mergeTranslations({
            test: {
                ru: 'заменен',
                en: 'replaced'
            }
        });
        // @ts-ignore
        expect(store.store).toEqual({
            test: {
                ru: 'заменен',
                en: 'replaced'
            },
            another: {
                ru: 'еще',
                en: 'antoher'
            }
        });
    });

    test('Получение переводов', () => {
        const store = createTranslationsStore('ru', 'en');
        store.registerTranslations('тест')({
            en: 'test text',
            ru: 'тестовый текст'
        });
        expect(store.getTranslation('ru')('тест')).toBe('тестовый текст');
        expect(store.getTranslation('en')('тест')).toBe('test text');
    });
 });
