import { mapTranslations } from '../withTranslations';

describe('Модули к withMapTranslations', function() {
    test('Мапинг переводов из хранимых в файлах к виду, применияемому в store', () => {
        const ru = {
            order: 'заказ',
            panel: 'панель'
        };
        const en = {
            order: 'order',
            panel: 'panel'
        };

        const res = mapTranslations<'order' | 'panel'>({ ru, en});
        expect(res).toEqual({
            order: {
                en: 'order',
                ru: 'заказ'
            },
            panel: {
                en: 'panel',
                ru: 'панель'
            }
        });
    });

    test('Мапинг с префиксом для ключей', function() {
        const ru = {
            order: 'заказ'
        };
        const en = {
            order: 'order'
        };

        const res = mapTranslations({ en, ru }, 'prefix');

        expect(res).toEqual({
            'prefix/order': {
                en: 'order',
                ru: 'заказ'
            }
        });
    });
});
