import { IBaseProps } from 'src/typings';

interface IFlexMods {
    /** Направление в котором будут выстраиваться элементы композита */
    /** @default row */
    direction?: 'column' | 'row';
}

interface IFlexProps extends IBaseProps<IFlexMods> {
}

export { IFlexProps };
