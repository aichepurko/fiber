import React from 'react';
import { cn as cnBase } from '@bem-react/classname';

import { withClassName } from 'src/HOC/withClassname';

import { IFlexProps } from './types';

import './styles';

const cn = cnBase('Flex');

const cnItem = cn('Item');

const baseMods: IFlexProps['classMod'] = {
    direction: 'row',
};

const mapChildrenItem = (item: React.ReactNode, index?: number) => <div key={index} className={cnItem}>{item}</div>;

const BaseFlex: React.StatelessComponent<IFlexProps> = ({ children, className }) => {
    const mappedChildren = Array.isArray(children)
        ? children.map(mapChildrenItem)
        : mapChildrenItem(children);

    return <div className={className}>{mappedChildren}</div>;
};

/**
 * @description
 * Контейнер для композитного размещения элементов
 */
const Flex = withClassName<IFlexProps>(cn, baseMods)(BaseFlex);

export { Flex };
