import React from 'react';
import { cn as cnBase } from '@bem-react/classname';

import { Link } from 'src/ui/atoms/Link';
import { Image } from 'src/ui/atoms/Image';

import { ILinkImageProps } from './types';
import './styles';

const cn = cnBase('LinkImage');

const LinkImage: React.StatelessComponent<ILinkImageProps> = (props) => {
    const {
        src,
        className,
        children,
        contentAlign,
        style
    } = props;

    const content = !Boolean(children) ?
        null :
        <div className={cn('Content')}>{children}</div>;

    return (
        <Link className={cn({align: contentAlign}, [className])} style={style}>
            <Image src={src} className={cn('Image')} />
            {content}
        </Link>
    );

};

LinkImage.defaultProps = {
    contentAlign: 'row',
};

export { LinkImage };
