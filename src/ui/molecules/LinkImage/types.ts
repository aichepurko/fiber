import { Omit } from 'utility-types';
import { IBaseProps } from 'src/typings';
import { IImageProps } from 'src/ui/atoms/Image/types';
import { ILinkProps } from 'src/ui/atoms/Link/types';

type ExtendedProps = Omit<IImageProps, 'classMod'> & Omit<ILinkProps, 'classMod'>;

interface ILinkImageProps extends IBaseProps<{some: string}>, ExtendedProps {
    contentAlign?: 'row' | 'column';
}

export { ILinkImageProps };
