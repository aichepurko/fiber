import React from 'react';
import { moleculeStory } from 'src/helpers/stories';

import { LinkImage } from '.';

const examples = moleculeStory('LinkImage', module);

const image = (
  <LinkImage
      style={{ width: 100 }}
      src="https://pp.userapi.com/TcadHGKZZlW1b8mIAvZzNJXoCOzqT1YXR8slfA/0sSJizFNBZ4.jpg"
  />
);

examples
  .add('Ссылка с картинкой', () => image)
  .add('Ссылка с картинкой и текстов', () => (
    <LinkImage
      style={{ width: 100, flexDirection: 'column-reverse'}}
      src="https://pp.userapi.com/TcadHGKZZlW1b8mIAvZzNJXoCOzqT1YXR8slfA/0sSJizFNBZ4.jpg"
    >Тут текст
    </LinkImage>
  ));

export { image };
