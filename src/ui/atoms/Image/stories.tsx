import React from 'react';
import { atomStory } from 'src/helpers/stories';

import { Image } from '.';

const examples = atomStory('Image', module);

const image = (
  <Image
      className="someClass"
      style={{ width: 100 }}
      src="https://pp.userapi.com/TcadHGKZZlW1b8mIAvZzNJXoCOzqT1YXR8slfA/0sSJizFNBZ4.jpg"
  />
);

examples
  .add('Дефолтная картинка', () => image);

export { image };
