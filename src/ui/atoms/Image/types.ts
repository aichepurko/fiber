import { IBaseProps } from 'src/typings';

interface IImageMods {
}

interface IImageProps extends IBaseProps<IImageMods> {
    /** Путь до изображения */
    src: string;
}

export { IImageProps };
