import React from 'react';
import { cn as cnBase } from '@bem-react/classname';
import { withClassName } from 'src/HOC/withClassname';

import { IImageProps } from './types';

const cn = cnBase('Image');

const baseMods: IImageProps['classMod'] = {};

const BaseImage: React.StatelessComponent<IImageProps> =
({src, className, style }) => <img style={style} className={className} src={src} />;

const Image = withClassName<IImageProps>(cn, baseMods)(BaseImage);

export { Image };
