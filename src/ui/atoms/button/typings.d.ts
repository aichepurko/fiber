import { IBaseProps } from 'src/typings/BaseProps';

interface IButtonProps extends IBaseProps {
    onCLick?: () => void;
}

export {
    IButtonProps
};
