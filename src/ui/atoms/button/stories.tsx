import React from 'react';
import { atomStory } from 'src/helpers/stories';
import { Button } from '.';

const examples = atomStory('Button', module);

examples
  .add('with text', () => (
    <Button className="someClass" classMod={{mod: 'val'}}>Hello Button</Button>
  ));

examples.add('another', () => <Button>Empty</Button>);
