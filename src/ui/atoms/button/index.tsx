import React from 'react';
import { cn as classname } from '@bem-react/classname';

import { withClassName } from 'src/HOC/withClassname';

import { IButtonProps } from './typings';

import './styles.pcss';

const cn = classname('Button');

const BaseButton: React.StatelessComponent<IButtonProps> = ({
    children,
    className,
    onCLick
}) => {
    return (
        <button className={className} onClick={onCLick}>
            <div className={cn('Content')}>{children}</div>
        </button>
    );
};

const Button = withClassName<IButtonProps>(cn)(BaseButton);

export {
    Button,
    cn as cnButton
};
