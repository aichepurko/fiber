import React from 'react';
import { atomStory } from 'src/helpers/stories';

import { Link } from '.';

const examples = atomStory('Link', module);

examples
  .add('Дефолтная ссылка', () => (
    <Link className="someClass">Link</Link>
  ));
