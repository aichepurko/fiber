import React from 'react';
// import { Link as ReactLink } from 'react-router-dom';
import { cn as cnBase } from '@bem-react/classname';

import { withClassName } from 'src/HOC/withClassname';
import { ILinkProps } from './types';

import './styles';

const cn = cnBase('Link');

const baseMods: ILinkProps['classMod'] = {};

const BaseLink: React.StatelessComponent<ILinkProps> = ({ children, className, to, style }) => {
    return <a style={style} className={className} href={to}>{children}</a>;
};

BaseLink.defaultProps = {
    to: '#'
};

const Link = withClassName<ILinkProps>(cn, baseMods)(BaseLink);

export { Link };
