import { IBaseProps } from 'src/typings';

interface ILinkMods {
    active?: boolean;
}

interface ILinkProps extends IBaseProps<ILinkMods> {
    to?: string;
}

export { ILinkProps };
