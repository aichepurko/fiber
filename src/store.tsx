import { createStore, combineReducers, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension/logOnlyInProduction';
import thunk from 'redux-thunk';

import { reducers } from './reducers';

const compose = composeWithDevTools({});

const store = createStore(
    combineReducers(reducers),
    compose(
        applyMiddleware(
            thunk
        )
    )
);

export {
    store
};
