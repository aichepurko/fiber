const allowedModes = [
    'development',
    'production',
    'testing'
];

const mode = allowedModes.includes(process.env.NODE_ENV) ? process.env.NODE_ENV : allowedModes[0];

module.exports = {
    context: __dirname,
    ...require(`./webpack/configs/${mode}.js`)
};
