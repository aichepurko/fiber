# Тестирование

## Unit-тестирование

Проводится при помощи [Jest](https://jestjs.io/docs/en/getting-started).

### Правила:

* Лежат в папках \_\_tests__


* Имя файла: *.test.js (ex: tests-module.test.js)
* Тесты к конкретному модулю лежат рядом с ним

[Пример](/src/__tests__/unit-example.test.js)

Запуск только юнит тестов:

```npm run test:unit```


## Регрессионное-тестирование

Для регрессионных тестов используем [Hermione](https://github.com/gemini-testing/hermione)

### Правила

* Имя файла: *.hermione.js (ex: feature-name.hermione.js)
* Каждая фича должна быть покрыта hermione тестами
* Тесты для фичи лежат в [features](/features)

Запукск регресс-тестов

```npm run test:hermione```
